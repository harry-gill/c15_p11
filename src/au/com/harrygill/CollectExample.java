package au.com.harrygill;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectExample {

    public static void main(String[] args) {

        Stream<String> stringStream = Stream.of("w", "o", "l", "f");

        //StringBuilder sb = stringStream.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);
        //System.out.println(sb);

        Set<String> result = stringStream.collect(Collectors.toCollection(TreeSet::new));
        System.out.println(result);
    }
}