package au.com.harrygill;

import java.util.stream.Stream;

public class CollectInfiniteStream {

    public static void main(String[] args) {

        Stream<String> infiniteStream = Stream.generate(String::new);

        StringBuilder sb = infiniteStream.collect(StringBuilder::new, StringBuilder::append, StringBuilder::append);

        System.out.println(sb);

    }
}